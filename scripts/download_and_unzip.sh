#!/usr/bin/env bash

# exit if anything fails.
set -e
set -u

DL_NAME="raspbian_lite_latest"
DL_URL="https://downloads.raspberrypi.org/$DL_NAME"

if [ "$#" -ne 2 ]; then
  echo "usage $0 <workdir> <new image name>"
  exit 1
else
  WORKDIR="$1"
  IMAGE_FILE="$2"
fi

if [ -d "$WORKDIR" ]; then
	echo "$WORKDIR exist - assuming cache is ok"
	echo "- delete it to redownload"
	exit 0
fi

mkdir "$WORKDIR"
cd "$WORKDIR"

echo "Downloading $DL_NAME from $DL_URL (if updated)"
wget -nv -N "$DL_URL"

IMAGE=$(unzip -l "$DL_NAME" | grep .img | cut -d ' ' -f 7)
if [ -f "$IMAGE" ]; then rm "$IMAGE"; fi

echo "- Image file is $IMAGE"
unzip "$DL_NAME"

echo "- renaming $IMAGE to $IMAGE_FILE"
mv "$IMAGE" "$IMAGE_FILE"

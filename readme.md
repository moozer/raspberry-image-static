Raspberry image static
=========================

The repo enables easy generation of raspberry pi images.

To use:

0. Install - see [below](#Installation)

1. Modify `scripts/copy_files.sh` to copy any relevant file to the new image
2. Modify `scripts/update_image.sh` and any commands you want to run on the image.

    This is run on "the raspberry", ie. in a similar environment. It relies on `proot` which allows for transparent run of armhf binaries on e.g. x86 hardware.

3. Run `make image`

    On my laptop this takes 10'ish minutes.

Other make option

* `make clean` deletes everything
* `make clean-cache` deletes the cache, which in turn make everything download again
* `make clean-build` deletes all build artifacts

Using it
-------

I suggest you fork+clone this repo, and make it fit your specific use case.

Mostly it will include adjusting `scripts/copy_files.sh` and `scripts/update_image.sh`. The current setup include my public key, you may want to change that to your own or those of your project group.

Default ip is 192.168.1.1. See [templates/ifcfg-interface-eth0](templates/ifcfg-interface-eth0)

Warning: Default is to push the resulting images to gitlab pages - if you don't want that, disable it in `.gitlab.ci.yml`

Installation
------------------------

You have two options: use `guestmount` of `mount -o loop ...`. If you use VMs and such on a regular linux box, then use `guestmount` otherwise use the `mount -o loop ...` as **root**. The latter is the one used with gitlab CI.

1. `git clone` this repo
2. Install `proot` and dependencies

    Usage e.g. `apt-get install zip proot qemu binfmt-support qemu-user-static`

3. (for `guestmount only`) Install guestfs tools

    Usage e.g. `apt-get install libguestfs-tools`

3. (for `guestmount only`) Add user to `KVM` group

    Usage e.g. `usermod -a -G kvm <user>`


Issues
-----------

* If you abort during the image building, you may have stale `fusermount`/`guestmount` on the `mnt` mountpoint.

    Check processes and use `fusermount -uz mnt` to force unmount

* Whenever the scripts are changed, the entire image is rebuild. Using something like `ansible` instead of scripts would be a way of handling this. I had massive issues with `ansible` and `proot`, so that part is on hold for now.


Bonus info
----------------------

The gitlab project includes pages where you may download the latest build. It is currently build monthly to ensure that it is always up-to-date.

Page are [here](http://moozer.gitlab.com/raspberry-image-static).


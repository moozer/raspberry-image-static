#!/usr/bin/env bash

# set fail modes
set -e
set -u

if [ "$#" -ne 1 ]; then
	echo "usage: $0 <imagefile>"
	exit 1
fi

IMAGE=$1
OFFSET=$(/sbin/fdisk -l "$IMAGE" | tail -n 1 | cut -c40-49)

echo "Splitting $IMAGE at offset $OFFSET"

dd if="$IMAGE" of=part1.img bs=512 count="$OFFSET"
dd if="$IMAGE" of=part2.img skip="$OFFSET" bs=512

ls -alh part?.img "$IMAGE"
